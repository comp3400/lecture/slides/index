<h6 style="font-size: small">Slides index</h6>

<section id="slides-index" class="slide">
<ul style="font-size: large; text-align: left">
  <li>
    <span style="font-size: large">
      [<a href="https://comp3400.gitlab.io/lecture/slides/revealjs-template/">slides</a>]
    </span>
    <span style="font-size: large">
      [<a href="https://gitlab.com/comp3400/lecture/slides/revealjs-template/">source</a>]
    </span>
    <span style="font-size: large">
      [<a href="https://gitlab.com/comp3400/lecture/slides/revealjs-template/issues/new">report issue</a>]
    </span>
    <span style="font-weight: bold">
      How to use the slide template
    </span>
  </li>
  <li>
    <span style="font-size: large">
      [<a href="https://comp3400.gitlab.io/lecture/slides/00-comp3400-overview/">slides</a>]
    </span>
    <span style="font-size: large">
      [<a href="https://gitlab.com/comp3400/lecture/slides/00-comp3400-overview/">source</a>]
    </span>
    <span style="font-size: large">
      [<a href="https://gitlab.com/comp3400/lecture/slides/00-comp3400-overview/issues/new">report issue</a>]
    </span>
    <span style="font-weight: bold">
      COMP3400 Overview
    </span>
  </li>
  <li>
    <span style="font-size: large">
      [<a href="https://comp3400.gitlab.io/lecture/slides/01-introduction-to-functional-programming/">slides</a>]
    </span>
    <span style="font-size: large">
      [<a href="https://gitlab.com/comp3400/lecture/slides/01-introduction-to-functional-programming/">source</a>]
    </span>
    <span style="font-size: large">
      [<a href="https://gitlab.com/comp3400/lecture/slides/01-introduction-to-functional-programming/issues/new">report issue</a>]
    </span>
    <span style="font-weight: bold">
      Introduction to Functional Programming
    </span>
  </li>
  <li>
    <span style="font-size: large">
      [<a href="https://comp3400.gitlab.io/lecture/slides/02-the-haskell-syntax/">slides</a>]
    </span>
    <span style="font-size: large">
      [<a href="https://gitlab.com/comp3400/lecture/slides/02-the-haskell-syntax/">source</a>]
    </span>
    <span style="font-size: large">
      [<a href="https://gitlab.com/comp3400/lecture/slides/02-the-haskell-syntax/issues/new">report issue</a>]
    </span>
    <span style="font-weight: bold">
      The Haskell Syntax
    </span>
  </li>
  <li>
    <span style="font-size: large">
      [<a href="https://comp3400.gitlab.io/lecture/slides/03-more-syntax-dev-techniques/">slides</a>]
    </span>
    <span style="font-size: large">
      [<a href="https://gitlab.com/comp3400/lecture/slides/03-more-syntax-dev-techniques/">source</a>]
    </span>
    <span style="font-size: large">
      [<a href="https://gitlab.com/comp3400/lecture/slides/03-more-syntax-dev-techniques/issues/new">report issue</a>]
    </span>
    <span style="font-weight: bold">
      More Haskell Syntax, Development Tools and Techniques
    </span>
  </li>
  <li>
    <span style="font-size: large">
      [<a href="https://comp3400.gitlab.io/lecture/slides/04-data-types-and-type-classes/">slides</a>]
    </span>
    <span style="font-size: large">
      [<a href="https://gitlab.com/comp3400/lecture/slides/04-data-types-and-type-classes/">source</a>]
    </span>
    <span style="font-size: large">
      [<a href="https://gitlab.com/comp3400/lecture/slides/04-data-types-and-type-classes/issues/new">report issue</a>]
    </span>
    <span style="font-weight: bold">
      Data Types and Type Classes
    </span>
  </li>
  <li>
    <span style="font-size: large">
      [<a href="https://comp3400.gitlab.io/lecture/slides/05-code-reuse/">slides</a>]
    </span>
    <span style="font-size: large">
      [<a href="https://gitlab.com/comp3400/lecture/slides/05-code-reuse/">source</a>]
    </span>
    <span style="font-size: large">
      [<a href="https://gitlab.com/comp3400/lecture/slides/05-code-reuse/issues/new">report issue</a>]
    </span>
    <span style="font-weight: bold">
      Code Reuse
    </span>
  </li>
  <li>
    <span style="font-size: large">
      [<a href="https://comp3400.gitlab.io/lecture/slides/06-abstraction-testing/">slides</a>]
    </span>
    <span style="font-size: large">
      [<a href="https://gitlab.com/comp3400/lecture/slides/06-abstraction-testing/">source</a>]
    </span>
    <span style="font-size: large">
      [<a href="https://gitlab.com/comp3400/lecture/slides/06-abstraction-testing/issues/new">report issue</a>]
    </span>
    <span style="font-weight: bold">
      Abstraction, More Code Reuse and Testing
    </span>
  </li>
  <li>
    <span style="font-size: large">
      [<a href="https://comp3400.gitlab.io/lecture/slides/07-monad-io-parser/">slides</a>]
    </span>
    <span style="font-size: large">
      [<a href="https://gitlab.com/comp3400/lecture/slides/07-monad-io-parser/">source</a>]
    </span>
    <span style="font-size: large">
      [<a href="https://gitlab.com/comp3400/lecture/slides/07-monad-io-parser/issues/new">report issue</a>]
    </span>
    <span style="font-weight: bold">
      Monad, IO, do-notation, parsers
    </span>
  </li>
  <li>
    <span style="font-size: large">
      [<a href="https://comp3400.gitlab.io/lecture/slides/08-revision-live-coding/">index</a>]
    </span>
    <span style="font-size: large">
      [<a href="https://gitlab.com/comp3400/lecture/slides/08-revision-live-coding/">source</a>]
    </span>
    <span style="font-size: large">
      [<a href="https://gitlab.com/comp3400/lecture/slides/08-revision-live-coding/issues/new">report issue</a>]
    </span>
    <span style="font-weight: bold">
      mid-semester revision &mdash; live coding of previous lecture examples 
    </span>
  </li>
  <li>
    <span style="font-size: large">
      [<a href="https://comp3400.gitlab.io/lecture/slides/09-monad-revision-monad-transformers/">index</a>]
    </span>
    <span style="font-size: large">
      [<a href="https://gitlab.com/comp3400/lecture/slides/09-monad-revision-monad-transformers/">source</a>]
    </span>
    <span style="font-size: large">
      [<a href="https://gitlab.com/comp3400/lecture/slides/09-monad-revision-monad-transformers/issues/new">report issue</a>]
    </span>
    <span style="font-weight: bold">
      Monad revision, Monad Transformers
    </span>
  </li>
  <li>
    <span style="font-size: large">
      [<a href="https://comp3400.gitlab.io/lecture/slides/10-monad-transformers-monoids-folds-traversable/">index</a>]
    </span>
    <span style="font-size: large">
      [<a href="https://gitlab.com/comp3400/lecture/slides/10-monad-transformers-monoids-folds-traversable/">source</a>]
    </span>
    <span style="font-size: large">
      [<a href="https://gitlab.com/comp3400/lecture/slides/10-monad-transformers-monoids-folds-traversable/issues/new">report issue</a>]
    </span>
    <span style="font-weight: bold">
      Monad Transformer revision, Monoids, Folds, Traversable
    </span>
  </li>
  <li>
    <span style="font-size: large">
      [<a href="https://comp3400.gitlab.io/lecture/slides/11-folds-adts-type-classes-tools/">index</a>]
    </span>
    <span style="font-size: large">
      [<a href="https://gitlab.com/comp3400/lecture/slides/11-folds-adts-type-classes-tools/">source</a>]
    </span>
    <span style="font-size: large">
      [<a href="https://gitlab.com/comp3400/lecture/slides/11-folds-adts-type-classes-tools/issues/new">report issue</a>]
    </span>
    <span style="font-weight: bold">
      The Algebra of Algebraic Data Types, More type classes, More Haskell tools
    </span>
  </li>
  <li>
    <span style="font-size: large">
      [<a href="https://comp3400.gitlab.io/lecture/slides/12-optics-data-types/">index</a>]
    </span>
    <span style="font-size: large">
      [<a href="https://gitlab.com/comp3400/lecture/slides/12-optics-data-types/">source</a>]
    </span>
    <span style="font-size: large">
      [<a href="https://gitlab.com/comp3400/lecture/slides/12-optics-data-types/issues/new">report issue</a>]
    </span>
    <span style="font-weight: bold">
      Optics and Data Type manipulation
    </span>
  </li>

</ul>
</section>
